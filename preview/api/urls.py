from django.urls import path

from . import views

app_name = 'preview'

urlpatterns = [
    path('from/markdown/to/html', views.FromMarkdownToHtml.as_view(), name='from_markdown_to_html'),
    # path('document/<uuid:uid>', views.DocView.as_view(), name='doc_view'),
    # path('document/<int:pk>', views.DocView.as_view(), name='doc_view'),
    # path('document/<int:pk>/log/viewing', views.DocLogViewingView.as_view(), name='doc_log_viewing_view'),
    # path('document/<uuid:uid>/log/viewing', views.DocLogViewingView.as_view(), name='doc_log_viewing_view'),
]
