from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from converter.utils import to_html


class FromMarkdownToHtml(APIView):
    def post(self, request, *args, **kwargs):
        md_text = request.POST.get('markdown_content', '')
        uid = request.POST.get('uid')
        html = to_html(md_text, uid)
        return Response(html, status=status.HTTP_200_OK, headers={})
