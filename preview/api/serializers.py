import re

from django.conf import settings
from django.db.models import Count
from log.models import Viewing as LogViewing
from markdown import Markdown
from rest_framework import serializers
from storage.api.serializers import DocumentSerializerRaw


class DocSerializer(serializers.ModelSerializer):
    h1_template = '<div class="row"><div class="col w100"><div id="content0" class="journal-item mt-0"><h1 {}</div></div></div><div class="gray-line"></div>'

    def from_markdown(self, instance):
        md = Markdown(extensions=['toc', 'tables', 'footnotes'])
        md_text = (settings.STORAGE_ROOT / str(instance.uid) / 'index.md').read_text(encoding='UTF-8')

        md_text = md_text.replace('\\</=', '&le;')
        md_text = md_text.replace('\\<', '&lt;')
        md_text = md_text.replace('\\>/=', '&ge;')
        md_text = md_text.replace('\\>', '&gt;')

        html = md.convert(md_text)
        toc = md.toc
        a_html = []
        tags = r'^(h1|h2|h3|h4|h5|h6|h7|h8|h9|p|table|ul|ol|li|dl|dd|dt|td)'
        i = 1
        for x in html.split('<'):
            if re.search(tags, x):
                x = re.sub(tags, r'\1 data-spid="{}"'.format(i), x)
                i += 1
            a_html.append(x)
        html = '<'.join(a_html)
        return {
            'html': html,
            'toc': toc,
            'doc': DocumentSerializerRaw(instance=instance).data,
        }

    def to_representation(self, instance):
        data = self.from_markdown(instance)
        html, toc = data['html'], data['toc']
        h1 = [self.h1_template.format(x.strip()) for x in html.split('<h1') if x.strip() != '']
        html = '\n'.join(h1)
        html = re.sub(r'<img alt="([^"]+)" src="([^"]+)"', '<a href="\\2" data-lightbox="\\1"><img src="\\2" alt="\\1"/></a', html)
        html = re.sub(r'(href|src)="([-_0-9a-zA-Z.а-яА-Я]+)"', '\\1="/storage/static/{}/\\2"'.format(instance.uid), html)
        html = html.replace('<a class="footnote-backref"', ' <a class="footnote-backref"')
        html = html.replace('<table', '<div style="overflow:auto"><table')
        html = html.replace('</table>', '</table></div>')
        return {
            'html': html,
            'toc': toc,
        }


class DocViewingLogSerializer(DocSerializer):
    def to_representation(self, instance):
        data = self.from_markdown(instance)
        html = data['html'].replace('\n', '')

        html = re.sub(r'(href|src)="([-_0-9a-zA-Z.а-яА-Я]+)"', '\\1="https://spnavigator.ru/storage/static/{}/\\2"'.format(instance.uid), html)
        html = re.sub(r'<img', '<img width="100%"', html)

        tags_child = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'h9', 'p', 'li', 'dl', 'dd', 'td']
        spid_tag = {int(x[1]): x[0] for x in re.findall(r'<(' + '|'.join(tags_child) + ')\s+data-spid="(\d+)"', html)}
        element_id_list = list(
            instance.log_viewing.filter(status=LogViewing.STATUS_VIEWING).values_list('element_id_min_child', 'element_id_max_child').annotate(cnt=Count('id')))

        # min_element_id, max_element_id = min([min(x[0], x[1]) for x in element_id_list]), max([max(x[0], x[1]) for x in element_id_list])
        max_element_id = max([max(x[0], x[1]) for x in element_id_list])
        # full_element_id = [0 for x in range(0, max_element_id + 1)]
        # full_element_id_cnt = [0] * (max_element_id + 1)
        full_element_id_cnt = {}
        for element_id_range in element_id_list:
            for element_id in range(element_id_range[0], element_id_range[1] + 1):
                element_id = int(element_id)
                if element_id in spid_tag.keys():
                    if element_id not in full_element_id_cnt:
                        full_element_id_cnt[element_id] = 0
                    full_element_id_cnt[element_id] += element_id_range[2]
        full_element_id_percent = {x: y / max(full_element_id_cnt.values()) for x, y in full_element_id_cnt.items()}
        data['element_id_cnt'] = full_element_id_cnt
        data['element_id_percent'] = {x: round(y * 100 * 1000) / 1000 for x, y in full_element_id_percent.items()}

        tags_child0 = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'h9', 'p', 'ul', 'dl', 'table', ]
        a_html = []
        last_x = ''
        for x in re.sub('(' + ('<' + '|<'.join(tags_child0) + '') + ')', r'$$$$\1', html).split('$$$$'):
            if not x:
                continue

            xx = None

            if '<ul' in last_x or '<ul' in x:
                last_x += x
                if '</ul' in x:
                    xx = last_x
                    last_x = ''
            elif '<ol' in last_x or '<ol' in x:
                last_x += x
                if '</ol' in x:
                    xx = last_x
                    last_x = ''
            else:
                last_x = ''
                xx = x
            # if s:
            #     s += x
            #     if '</tr' in x:
            #         xx = s
            # else:
            #     if '<tr' in x:
            #         s = x
            #     else:
            #         xx = x
            # xx = x
            if xx:
                s = None
                try:
                    spid = int(re.findall(r'<(' + '|'.join(tags_child) + ')\s+data-spid="(\d+)"', xx)[0][1])
                    a_html.append([spid, xx])
                except KeyError:
                    a_html.append([None, xx])

        data['element_id_chars'] = {x: len(re.sub(r'<[^>]+>', '', y)) for x, y in a_html if x}
        a_html_element_id_percent = {id[0]: full_element_id_percent[id[0]] for id in a_html}
        full_element_id_percent_ = {x: y / sum(a_html_element_id_percent.values()) for x, y in a_html_element_id_percent.items()}
        data['element_id_percent_'] = {x: round(y * 100 * 100) / 100 for x, y in full_element_id_percent_.items()}
        data['element_id_rating'] = {x[1][0]: x[0] for x in enumerate(sorted(a_html_element_id_percent.items(), key=lambda x: x[1], reverse=True), start=1)}
        data['a_html'] = a_html

        return data
