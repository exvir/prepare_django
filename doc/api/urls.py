from django.urls import path

from . import views

app_name = 'doc'

urlpatterns = [
    path('from/markdown/to/html', views.FromMarkdownToHtml.as_view(), name='from_markdown_to_html'),
    path('markdown/<int:pk>/save', views.MarkdownSave.as_view(), name='markdown_save'),
    path('markdown/<int:pk>/load', views.MarkdownLoad.as_view(), name='markdown_load'),
    path('markdown/navigator/<uuid:pk>/save', views.MarkdownNavigatorSave.as_view(), name='markdown_navigator_save'),
    path('markdown/navigator/<uuid:pk>/load', views.MarkdownNavigatorLoad.as_view(), name='markdown_navigator_load'),
    # path('document/<uuid:uid>', views.DocView.as_view(), name='doc_view'),
    # path('document/<int:pk>', views.DocView.as_view(), name='doc_view'),
    # path('document/<int:pk>/log/viewing', views.DocLogViewingView.as_view(), name='doc_log_viewing_view'),
    # path('document/<uuid:uid>/log/viewing', views.DocLogViewingView.as_view(), name='doc_log_viewing_view'),
]
