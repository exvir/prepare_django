import json

import urllib3
from django.conf import settings
from rest_framework import status
from rest_framework.generics import UpdateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from converter.utils import to_html
from .. import models


class FromMarkdownToHtml(APIView):
    def post(self, request, *args, **kwargs):
        md_text = request.POST.get('markdown_content', '')
        uid = request.POST.get('uid')
        html = to_html(md_text, uid)
        return Response(html, status=status.HTTP_200_OK, headers={})


class MarkdownDictObjectMixin(object):
    def get_object_dict(self):
        self.object = self.get_object()
        data = dict(
            markdown_content=self.object.get_markdown_filepath().read_text(encoding='utf-8'),
            markdown_prefix=self.object.get_media_url(),
            name=self.object.name,
            status=self.object.status,
        )
        return data


class MarkdownLoad(MarkdownDictObjectMixin, RetrieveAPIView):
    queryset = models.Doc.objects

    def retrieve(self, request, *args, **kwargs):
        data = self.get_object_dict()
        return Response(data, status=status.HTTP_200_OK)


class MarkdownSave(MarkdownDictObjectMixin, UpdateAPIView):
    queryset = models.Doc.objects

    def update(self, request, *args, **kwargs):
        doc = self.get_object()
        if 'markdown_content' in request.data:
            markdown_content = request.data.get('markdown_content')
            if not markdown_content:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            doc.get_markdown_filepath().write_text(markdown_content, encoding='utf-8')
            doc.status = models.Doc.STATUS_WORK
            doc.save()
        elif 'status' in request.data:
            doc.status = request.data['status']
            doc.save()
        data = self.get_object_dict()
        return Response(data, status=status.HTTP_200_OK)


class MarkdownNavigatorLoad(RetrieveAPIView):
    def retrieve(self, request, *args, **kwargs):
        pool = urllib3.PoolManager()
        url = settings.SPNAVIGATOR_API.format(url='storage/document/{uid}/get/for/editor'.format(uid=self.kwargs['pk']))
        res = pool.request('GET', url, headers={'Authorization': 'Token39 {}'.format(self.request.user.profile.token39)})
        data = json.loads(res.data.decode('utf-8'))
        markdown_content = data['data']
        markdown_prefix = settings.SPNAVIGATOR_BASE.format(url=data['src_prefix'])
        return Response({'markdown_content': markdown_content, 'markdown_prefix': markdown_prefix}, status=status.HTTP_200_OK)


class MarkdownNavigatorSave(UpdateAPIView):
    queryset = models.Doc.objects

    def update(self, request, *args, **kwargs):
        doc = self.get_object()
        markdown_content = request.data.get('markdown_content')
        if not markdown_content:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        doc.get_markdown_filepath().write_text(markdown_content, encoding='utf-8')
        return Response({'markdown_content': markdown_content}, status=status.HTTP_200_OK)
