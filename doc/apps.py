from django.apps import AppConfig


class DocConfig(AppConfig):
    name = 'doc'
    verbose_name = 'Документ'
    verbose_name_plural = 'Документы'
