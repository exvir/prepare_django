from django.urls import path

from . import views

app_name = 'doc'

urlpatterns = [
    path('create', views.DocCreateView.as_view(), name='create'),
    path('<int:pk>/update', views.DocUpdateView.as_view(), name='update'),
    path('<int:pk>/download', views.DocDownloadView.as_view(), name='download'),
    path('<int:pk>/editor', views.DocEditorView.as_view(), name='editor'),
    path('', views.DocListView.as_view(), name='list'),
    path('navigator', views.DocNavigatorList.as_view(), name='navigator_list'),
    path('navigator/<str:pk>/editor', views.DocNavigatorEditorView.as_view(), name='navigator_editor'),
    path('filter', views.FilteredDocListView.as_view(), name='filter')
]
