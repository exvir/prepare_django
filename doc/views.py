import json
import re
import shutil
from io import BytesIO
from operator import itemgetter
from zipfile import ZipFile

import urllib3
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import ChoiceField
from django.http import HttpResponse
from django.urls import reverse_lazy, reverse
from django.utils import dateparse
from django.views.generic import CreateView, UpdateView, ListView, DetailView, TemplateView
from rest_framework import status

from converter.utils import convert
from core.utils import rfc5987_content_disposition
from . import forms
from . import models


class DocListView(LoginRequiredMixin, ListView):
    template_name = 'doc/list.html'

    def get_queryset(self):
        queryset = models.Doc.objects
        if not self.request.user.is_superuser:
            queryset = queryset.filter(owner=self.request.user)
        return queryset.order_by('-modified')

    def get_context_data(self, **kwargs):
        context = super(DocListView, self).get_context_data(**kwargs)
        context['filter'] = forms.DocFilterForm(self.request.GET)
        return context	


class FilteredDocListView(DocListView):

    def get_queryset(self):
        queryset = super().get_queryset()
        filtred = forms.DocFilterForm(self.request.GET, queryset=queryset)
        return filtred.qs


class DocExtractMixin(object):
    # TODO разобраться с разархивацией и переименованием файлов на латиницу
    def extract_file(self, doc):
        path = doc.get_storage_path()
        with ZipFile(doc.file_archive.file, 'r') as z:
            z.extractall(path)
            for name in z.namelist():
                unicode_name = name.encode('cp437').decode('cp866')
                fullpath = path / unicode_name
                if z.getinfo(name).is_dir():
                    fullpath.mkdir(parents=True, exist_ok=True)
                else:
                    with z.open(name) as f:
                        content = f.read()
                        with open(fullpath, 'wb') as f:
                            f.write(content)
        in_file = list(path.glob('*.html'))[0]
        media_path = doc.get_media_path()
        media_path.mkdir(parents=True, exist_ok=True)
        convert_result = convert(in_file, str(media_path))
        doc.extra_data['convert_result'] = convert_result
        markdown_path = doc.get_markdown_path()
        markdown_path.mkdir(parents=True, exist_ok=True)
        shutil.copy(media_path / 'index.md', markdown_path / 'index.md')
        # (media_path / 'index.md').unlink()


class DocCreateView(LoginRequiredMixin, DocExtractMixin, CreateView):
    queryset = models.Doc.objects
    form_class = forms.DocForm
    template_name = 'doc/create.html'
    success_url = reverse_lazy('doc:list')

    # def get_initial(self):
    #     return {'owner': self.request.user}

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.instance.status = models.Doc.STATUS_UPLOADED
        ret = super().form_valid(form)
        self.extract_file(form.instance)
        form.instance.save()
        return ret


class DocUpdateView(LoginRequiredMixin, DocExtractMixin, UpdateView):
    queryset = models.Doc.objects
    form_class = forms.DocForm
    template_name = 'doc/update.html'

    # success_url = reverse_lazy('doc:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'convert_result' in self.object.extra_data and \
                'unknown_colors' in self.object.extra_data['convert_result'] and \
                self.object.extra_data['convert_result']['unknown_colors']:
            context['unknown_colors'] = [(x, ChoiceField(choices=('---', 'Полужирный', 'Крик', 'Врезка')))
                                         for x in self.object.extra_data['convert_result']['unknown_colors']]
        return context

    def form_valid(self, form):
        form.instance.status = models.Doc.STATUS_UPLOADED if form.instance.file_archive else models.Doc.STATUS_NOTHING
        ret = super().form_valid(form)
        self.extract_file(form.instance)
        form.instance.save()
        return ret

    def get_success_url(self):
        if 'convert_result' in self.object.extra_data and \
                'unknown_colors' in self.object.extra_data['convert_result'] and \
                self.object.extra_data['convert_result']['unknown_colors']:
            return reverse('doc:update', args=[self.object.id, ])
        else:
            return reverse('doc:editor', args=[self.object.id, ])


class DocEditorView(LoginRequiredMixin, DocExtractMixin, DetailView):
    queryset = models.Doc.objects
    template_name = 'doc/editor.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['markdown_content'] = (self.object.get_markdown_path() / 'index.md').read_text(encoding='utf-8')
        # context['markdown_media'] = self.object.get_media_url()
        context['markdown_load_action'] = reverse('api:doc:markdown_load', args=[self.kwargs['pk'], ])
        context['markdown_save_action'] = reverse('api:doc:markdown_save', args=[self.kwargs['pk'], ])
        return context


class DocNavigatorList(LoginRequiredMixin, ListView):
    template_name = 'doc/navigator_list.html'

    def get_queryset(self):
        pool = urllib3.PoolManager()
        url = settings.SPNAVIGATOR_API.format(url='storage/document')
        res = pool.request('GET', url, headers={'Authorization': 'Token39 {}'.format(self.request.user.profile.token39)})
        data = json.loads(res.data.decode('utf-8'))
        if res.status == status.HTTP_200_OK:
            for doc in data:
                if doc['created']:
                    doc['created'] = dateparse.parse_datetime(doc['created'])
                if doc['modified']:
                    doc['modified'] = dateparse.parse_datetime(doc['modified'])
            return data
        else:
            if 'detail' in data:
                messages.error(self.request, 'Ошибка: {}'.format(data['detail']))
            else:
                messages.error(self.request, 'Неизвестная ошибка')
            return []

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['doc__category__code__list'] = sorted(list(set([(x['category']['id'], x['category']['name']) for x in self.object_list])), key=itemgetter(1))
        return context


class DocNavigatorEditorView(LoginRequiredMixin, TemplateView):
    template_name = 'doc/editor.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['markdown_load_action'] = reverse('api:doc:markdown_navigator_load', args=[self.kwargs['pk'], ])
        context['markdown_save_action'] = reverse('api:doc:markdown_navigator_save', args=[self.kwargs['pk'], ])
        return context


class DocDownloadView(LoginRequiredMixin, DetailView):
    queryset = models.Doc.objects

    def get(self, request, *args, **kwargs):
        doc = self.get_object()
        md = doc.get_markdown_filepath().read_text('utf-8')
        images = [x.groups() for x in re.finditer(r'!\[[^\]]*\]\(([^\)]+)\.(\w+)\)', md)]
        zip_stream = BytesIO()
        with ZipFile(zip_stream, 'a') as f:
            f.write(doc.get_markdown_filepath(), 'index.md')
            for name, ext in images:
                filename = '{}.{}'.format(name, ext)
                f.write(doc.get_media_path() / filename, filename)
        zip_stream.seek(0)
        response = HttpResponse(zip_stream, content_type='application/zip')
        response['Content-Disposition'] = rfc5987_content_disposition(doc.name.replace(" ", "_") + '.zip')
        return response
