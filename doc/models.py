from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import ForeignKey, CharField, FileField, Model, PROTECT, SmallIntegerField
from django_mysql.models import JSONField
from model_utils.models import TimeStampedModel
from simple_history.models import HistoricalRecords

from core.models import CodeNameModel, NameModel


class Category(CodeNameModel):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Project(CodeNameModel):
    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'


class Doc(TimeStampedModel, NameModel):
    STATUS_NOTHING = 0
    STATUS_UPLOADED = 1
    STATUS_WORK = 2
    STATUS_PREPARED = 3
    STATUS_ERRORS = 4
    STATUS_COMPLETE = 5
    STATUS_CHOICES = (
        (STATUS_NOTHING, 'ничего'),
        (STATUS_UPLOADED, 'загружено'),
        (STATUS_WORK, 'в работе'),
        (STATUS_PREPARED, 'подготовлено'),
        (STATUS_ERRORS, 'ошибки'),
        (STATUS_COMPLETE, 'выполнено'),
    )
    project = ForeignKey(Project, related_name='docs', verbose_name='Проекты', on_delete=PROTECT, default=1)
    owner = ForeignKey(get_user_model(), related_name='docs', verbose_name='Владелец', on_delete=PROTECT)
    name = CharField('Название', max_length=256)
    category = ForeignKey(Category, related_name='docs', verbose_name='Категория', on_delete=PROTECT)
    file_archive = FileField('Архивный файл', upload_to='uploads/%Y%m%d/', null=True)
    status = SmallIntegerField('Статус', default=STATUS_NOTHING, choices=STATUS_CHOICES)
    history = HistoricalRecords()
    extra_data = JSONField(default=dict)

    def get_markdown_path(self):
        return settings.STORAGE_ROOT / 'markdown' / str(self.id)

    def get_markdown_filepath(self):
        return self.get_markdown_path() / 'index.md'

    def get_bibliography_filepath(self):
        return self.get_markdown_path() / 'bibliography.md'

    def get_media_path(self):
        return settings.MEDIA_ROOT / 'markdown' / str(self.id)

    def get_media_url(self):
        return '{}markdown/{}/'.format(settings.MEDIA_URL, str(self.id))

    def get_storage_path(self):
        return settings.STORAGE_ROOT / 'indesign' / str(self.id)

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
