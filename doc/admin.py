from django.contrib import admin

from . import models


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    model = models.Category
    list_display = ('id', 'code', 'name')
    list_display_links = ('id', 'code', 'name')


@admin.register(models.Doc)
class DocAdmin(admin.ModelAdmin):
    model = models.Doc
    list_display = ('id', 'category', 'project', 'status', 'name', 'created', 'modified', 'file_archive',)
    list_display_links = ('id', 'category', 'status', 'name',)
    list_filter = ('category',)

@admin.register(models.Project)
class DocAdmin(admin.ModelAdmin):
    model = models.Project