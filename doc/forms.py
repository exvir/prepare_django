from django.forms import ModelForm

import django_filters

from . import models



class DocForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = models.Doc
        fields = ('name', 'project', 'file_archive', 'category',)

class DocFilterForm(django_filters.FilterSet):
    class Meta:
        model = models.Doc
        fields = ['status', 'project']
