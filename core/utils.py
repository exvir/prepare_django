import unicodedata

from django.utils.http import urlquote


def rfc5987_content_disposition(filename):
    ascii_name = unicodedata.normalize('NFKD', filename).encode('ascii', 'ignore').decode()
    header = 'attachment; filename="{}"'.format(ascii_name)
    if ascii_name != filename:
        quoted_name = urlquote(filename)
        header += '; filename*=UTF-8\'\'{}'.format(quoted_name)

    return header

