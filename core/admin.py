from django.contrib import admin

from . import models


@admin.register(models.PairSettings)
class PairSettingsAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'value')
    list_display_links = ('code', 'name', 'value')
