import uuid

from django.db import models
from django.utils.translation import gettext as _
from django_mysql.models import JSONField
from model_utils.models import TimeStampedModel


# from django.contrib.postgres.fields import JSONField

class ShortNameModel(models.Model):
    """
    Объект с кратким именем

    """
    short_name = models.CharField(_('Краткое наименование'), max_length=128, blank=True, null=True)

    class Meta:
        abstract = True


class NameModel(models.Model):
    """
    Объект с именем
    """
    name = models.CharField(_('Наименование'), max_length=4096)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class CodeNameModel(NameModel):
    """
    Объект с именем и кодом

    """
    code = models.CharField(_('Код'), max_length=256, blank=True)

    def __str__(self):
        return self.name if self.code == '' else self.code

    class Meta:
        abstract = True


class UidModel(models.Model):
    """
    Объект с уникальным идентификатором
    """
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        abstract = True


class IsActive(models.Model):
    """
    Активность
    """
    is_active = models.BooleanField('Активно', default=True)

    class Meta:
        abstract = True


class PairSettings(CodeNameModel, TimeStampedModel):
    """
    Параметры приложения в формате
    code|name -> value [<options>]
    code|name -> json

    """
    value = models.TextField()
    json = JSONField(blank=True, null=True)

    @staticmethod
    def get_value(code):
        return PairSettings.objects.get(code=code).value

    @staticmethod
    def set_value(code, value):
        o, _ = PairSettings.objects.get_or_create(code=code)
        o.value = value
        o.save()
        return o

    @staticmethod
    def get_json(code):
        return PairSettings.objects.get(code=code).json

    @staticmethod
    def set_json(code, data):
        o, _ = PairSettings.objects.get_or_create(code=code)
        o.json = data
        o.save()
        return o

    @staticmethod
    def get_options(code):
        return PairSettings.objects.get(code=code).options

    @staticmethod
    def set_options(code, options):
        o = PairSettings.objects.get_or_create(code=code)
        o.options = options
        o.save()
        return o

    class Meta:
        verbose_name = 'Параметр'
        verbose_name_plural = 'Параметры'
