import json
import re
import shutil
from io import StringIO
from pathlib import Path

import cssutils
import markdown
from lxml import etree

from core.models import PairSettings
from .ocxsect import OCXMetadata


def to_html(md_text, uid=''):
    md = markdown.Markdown(extensions=['toc', 'tables', 'footnotes', 'fenced_code', OCXMetadata()])
    # html = md.convert((settings.STORAGE_ROOT / str(self.object.uid) / 'index.md').read_text(encoding='UTF-8'))
    html = md.convert(md_text)
    toc = md.toc
    html = re.sub(r'<img alt="([^"]+)" src="([^"]+)"', r'<a href="\2" data-lightbox="\1"><img src="\2" alt="\1"/></a', html)
    html = re.sub(r'(href|src)="([-_0-9a-zA-Z.а-яА-Я]+)"', r'\1="/media/markdown/{}/\2"'.format(uid), html)
    html = html.replace('<a class="footnote-backref"', ' <a class="footnote-backref"')
    html = html.replace('<table', '<div style="overflow:auto"><table')
    html = html.replace('</table>', '</table></div>')
    html = html.replace('<p>***</p>', '<p style="text-align: center"><img src="/static/img/sp/stars.png"/></p>')
    a_html = []
    tags = r'^(h1|h2|h3|h4|h5|h6|h7|h8|h9|p|table|ul|ol|li|dl|dd|dt|td)'
    i = 1
    for x in html.split('<'):
        if re.search(tags, x):
            x = re.sub(tags, r'\1 data-spid="{}"'.format(i), x)
            i += 1
        a_html.append(x)
    html = '<'.join(a_html)
    return html


class Replaces(list):
    def append_tag(self, tag, class_name, regexp):
        self.append(('<{tag}\\s+class="{class_name}">(.+?)</{tag}>'.format(tag=tag, class_name=class_name), regexp,))
        return self

    def replace(self, html):
        for x, y in self:
            html = re.sub(x, y, html, flags=re.MULTILINE | re.IGNORECASE | re.UNICODE | re.DOTALL)
        return html

    def append_list(self, lst):
        for l in lst:
            self.append(l)
        return self


def element_text_wrap_up(element, before=None, after=None):
    if before is not None:
        if element.text is None:
            element.text = before
        else:
            element.text = before + element.text
    if after is not None:
        if len(element.getchildren()) > 0:
            if element.getchildren()[-1].tail is None:
                element.getchildren()[-1].tail = after
            else:
                element.getchildren()[-1].tail += after
        else:
            if element.text is None:
                element.text = after
            else:
                element.text += after
    return element


def convert(in_file, out_folder):
    classes_bad = ['CellOverride-1', 'CellOverride-2', 'CellOverride-3', 'CellOverride-4', 'CellOverride-5', 'CellOverride-6', 'CellOverride-7', 'CellOverride-8', 'CellOverride-9',
                   'CharOverride-1', 'CharOverride-10', 'CharOverride-11', 'CharOverride-12', 'CharOverride-13', 'CharOverride-14', 'CharOverride-15', 'CharOverride-2',
                   'CharOverride-3', 'CharOverride-4', 'CharOverride-5', 'CharOverride-6', 'CharOverride-7', 'CharOverride-8', 'CharOverride-9', 'ParaOverride-1',
                   'ParaOverride-10', 'ParaOverride-11', 'ParaOverride-2', 'ParaOverride-3', 'ParaOverride-4', 'ParaOverride-5', 'ParaOverride-6', 'ParaOverride-7',
                   'ParaOverride-8', 'ParaOverride-9', 'Tabl_body', '_-brkout-2-bold', '_idGenObjectAttribute-1', '_idGenObjectAttribute-2', '_idGenObjectAttribute-3',
                   '_idGenObjectLayout-1', '_idGenObjectStyleOverride-1', '_idGenObjectStyleOverride-2', '_idGenObjectStyleOverride-3', '_idGenObjectStyleOverride-4',
                   '_idGenTableRowColumn-3', '_idGenTableRowColumn-4', '_idGenTableRowColumn-5', '_idGenTableRowColumn-6', '_idGenTableRowColumn-7', '_•author', '_•body',
                   '_•body-list-magic-c', '_•body-list-magic-c_3', '_•body-nointend', '_•breakout-2', '_•footnote', '_•headln', '_•inset', '_•subheader',
                   '_•subheader_podZag-v-stat-e', 'Стандартная-таблица', 'Текстовый-фрейм', 'текст-вреза']

    src_path = in_file
    dst_path = Path(out_folder).resolve()
    html = src_path.read_text(encoding='utf-8')

    css_path = Path(in_file).resolve().parent / re.findall(r'href="([^"]+.css)"', html)[0]
    styles = [
        (
            x.selectorText.split('.')[0],
            x.selectorText.split('.')[1] if '.' in x.selectorText else '',
            y.name.lower(),
            y.value.lower(),
        )
        for rule in cssutils.parseFile(css_path).cssRules
        for x in rule.selectorList
        for y in rule.style
        if y.name in (
            'background',
            'background-color',
            'color',
            'vertical-align',
        ) and x.selectorText.split('.')[0] not in (
               'td',
               'tr',
               'table',
           ) and y.value.lower() not in (
               '#000',
               '#fff',
               '#000000',
               '#ffffff',
               'black',
               'white',
           )
    ]

    replaces = Replaces([
        (r'(?:\r|\n)', ' '),
        (r'<!DOCTYPE[^>]+>', ''),
        (r'xmlns(:\w+)?="[^"]+"', ''),
        (r'(\*)', r'\*'),
        (r'<span>\s+', ' <span>'),
        (r'\s+</span>', '</span> '),
        # (r'id="[^"]+"', r''),
        # (r'<span[^>]+[^>]+цвет-в-тексте[^>]+>([^<]+)</span>', r'<strong>\1</strong>'),
    ])
    html = replaces.replace(html)

    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(html), parser)

    classes_doc = set([s for e in tree.xpath('//*[@class]') for s in e.attrib['class'].split(' ')])

    classes_subst = PairSettings.get_json('converter.classes.subst')
    """
    [
        ('_•author', 'author', '~~D author~~\n\\1\n~~/D~~'),
        ('_•footnote', 'footnote', '~~D footnote~~\n\\1\n~~/D~~'),
        ('_•inset', 'lead', '~~D lead~~\n\\1\n~~/D~~'),
        ('blockquote', 'blockquote', '~~D blockquote~~\n\\1\n~~/D~~'),
        ('_•headln', 'h1', '# \\1'),
        ('_•subheader', 'h2', '## \\1'),
        ('_•subheader_podZag-v-stat-e', 'subtitle', '~~D subtitle~~\n\\1\n~~/D~~'),
        ('strong', 'strong', r'**\1**'),
        ('bawl', 'bawl', '~~D bawl~~\n\\1\n~~/D~~'),
        ('ref', 'ref', '[^\\1]'),
    ]
    """

    classes_inline = PairSettings.get_json('converter.classes.inline')  # ['strong', 'ref', ]

    classes_bad = [x for x in classes_bad if x not in classes_subst]
    classes = [*[x[1] for x in styles], *[x[0] for x in classes_subst]]

    (dst_path / 'classes_doc.json').write_text(json.dumps(sorted(list(classes_doc)), ensure_ascii=False), encoding='utf-8')
    (dst_path / 'classes_bad.json').write_text(json.dumps(sorted(list(classes_bad)), ensure_ascii=False), encoding='utf-8')
    (dst_path / 'classes_subst.json').write_text(json.dumps(sorted(list(classes_subst)), ensure_ascii=False), encoding='utf-8')
    (dst_path / 'classes.json').write_text(json.dumps(sorted(list(classes)), ensure_ascii=False), encoding='utf-8')
    (dst_path / 'styles.json').write_text(json.dumps(sorted(list(styles)), ensure_ascii=False), encoding='utf-8')

    for e in tree.xpath('//*[@class]'):
        class_attr = [c for c in e.attrib['class'].split(' ') if c in classes]
        if len(class_attr) > 0:
            e.attrib['class'] = ' '.join(class_attr)
        else:
            del e.attrib['class']

    for e in tree.xpath('//*[@id]'):
        del e.attrib['id']

    for e in tree.xpath('//*[@lang]'):
        del e.attrib['lang']

    for e in tree.xpath('//colgroup|//head'):
        e.getparent().remove(e)

    for i, e in enumerate(tree.xpath('//img'), start=1):
        if 'src' not in e.attrib:
            continue
        src_img_path = src_path.parent / e.attrib['src']
        dst_img_src = '{}{}'.format(i, src_img_path.suffix)
        e.attrib['src'] = dst_img_src
        shutil.copy(str(src_img_path), str(dst_path / dst_img_src))

    colors_strong = PairSettings.get_json('converter.colors.strong')  # ['#b30838', '#970032', '#b20933', '' ]
    colors_bawl = PairSettings.get_json('converter.colors.bawl')  # ['#7296b7', '#74b0e2', '#a7b334', '']
    colors_blockquote = PairSettings.get_json('converter.colors.blockquote')  # ['#bbd7ed', '#b30838', ]
    unknown_colors = {}

    for tag, class_name, attr, value in styles:
        for e in tree.xpath('//*[@class="{}"]'.format(class_name)):
            if tag == 'span' and attr == 'vertical-align' and value == 'super':
                e.attrib['class'] = 'ref'
            elif tag == 'span' and attr == 'color' and value in colors_strong:
                e.attrib['class'] = 'strong'
            elif attr in ('background-color', 'background') and value in colors_blockquote:
                e.attrib['class'] = 'blockquote'
            elif attr in ('color',) and value in colors_bawl:
                e.attrib['class'] = 'bawl'
            else:
                if value not in unknown_colors:
                    unknown_colors[value] = []
                unknown_colors[value].append(dict(
                    style='{}: {}'.format(attr, value),
                    text=''.join(e.itertext())
                ))

    for src_class, class_name, _ in classes_subst:
        for e in tree.xpath('//*[@class="{}"]'.format(src_class)):
            e.attrib['class'] = class_name

    for e in tree.xpath('//span'):
        if not e.text:
            e.getparent().remove(e)

    html = etree.tostring(tree, pretty_print=False, encoding='utf-8').decode('utf-8')
    (dst_path / 'preindex2.html').write_text(html, encoding='utf-8')

    replaces = Replaces([
        (r'<!DOCTYPE[^>]+>', ''),
        (r'<head>(.+?)</head>', ''),
        (r'</?(?:body|html).*?>', ''),
        (r'>\s+<', r'><'),
        (r'<span>((?!</span>).+?)</span>', r'\1'),
        (r'<div><div>((?!</div></div>).+?)</div></div>', r'<div>\1</div>'),
        (r'<div><p((?!</p></div>).+?)</p></div>', r'<p\1</p>'),
        (r'<div><img([^>]+)/?></div>', r'<img\1>'),
        (r'<span[^>]*>\s*</span>', ''),
        (r'<span[^>]*>\s*(\[|\])\s*</span>', ''),
        (r'(/p>)(<img[^>]+/?>)(<p)', r'\1<p>\2</p>\3'),
        (r'(/div>)(<img[^>]+/?>)(<div)', r'\1<p>\2</p>\3'),
        (r'<img\s+src="([^"]+)"(?:.*?)/?>', r'![](\1)'),
        (r'(<th>)<p>((?!</p></th>).+?)</p>(</th>)', r'\1\2\3'),
        (r'(<td>)<p>((?!</p></td>).+?)</p>(</td>)', r'\1\2\3'),
    ])

    for _, class_name, regexp in classes_subst:
        if class_name in classes_inline:
            replaces.append_tag('span', class_name, regexp)
    replaces.append((r'\[\^(\d+)\s*,\s*([^\]]+)\s*\]', r'[^\1] [^\2]'))

    html = replaces.replace(html)

    tree = etree.parse(StringIO(html), parser)

    for table in tree.xpath('//table'):
        for row_n, tr in enumerate(table.xpath('tr|thead/tr|tbody/tr'), start=1):
            for cell_n, cell in enumerate(tr.xpath('th|td'), start=1):
                element_text_wrap_up(cell, before='| ' if cell_n == 1 else None, after=' | ')
            if row_n == 1:
                new_tr = etree.Element('tr')
                for cell_n, cell in enumerate(tr.xpath('th|td'), start=1):
                    new_td = etree.Element('td')
                    new_td.text = '---'
                    element_text_wrap_up(new_td, before='| ' if cell_n == 1 else None, after=' | ')
                    new_tr.append(new_td)
                tr.addnext(new_tr)
            row_n += 1

    for ul in tree.xpath('//ul'):
        for li in ul.xpath('li'):
            li.text = '- ' + li.text

    for ol in tree.xpath('//ol'):
        for li, i in enumerate(ol.xpath('li'), start=1):
            li.text = i + '. ' + li.text

    for _, class_name, regexp in classes_subst:
        for e in tree.xpath('//*[@class="{}"]'.format(class_name)):
            s = regexp.split('\\1')
            element_text_wrap_up(e, before=s[0], after=s[1] if len(s) > 1 else None)

    html = etree.tostring(tree, pretty_print=False, encoding='utf-8').decode('utf-8')
    (dst_path / 'preindex3.html').write_text(html, encoding='utf-8')

    replaces = Replaces([
        (r'\r?\n?<!DOCTYPE[^>]+>', ''),
        (r'\r?\n?<head>(.+?)</head>', ''),
        (r'\r?\n?</?(?:body|html|tbody|thead).*?>', ''),
        (r'</p[^>]*>(?:</span[^>]*>)?', ''),
        (r'<ul[^>]*>', '\n'),
        (r'</ul[^>]*>', ''),
        (r'<li[^>]*>', '\n'),
        (r'</li[^>]*>', ''),
        (r'<div[^>]*>((?!<p).*?)<p[^>]*>', '\n\n\\1'),
        (r'</p[^>]*>((?!</div).*?)</div[^>]*>', ''),
        (r'(?:</span[^>]*>)?</div[^>]*>', '\n\n'),
        (r'<div[^>]*>(?:<span[^>]*>)?', '\n\n'),
        (r'</span[^>]*/>((?!</p).*?)</p[^>]*>', '\\1\n\n'),
        (r'<p[^>]*>((?!<span).*?)<span[^>]*>', '\n\n\\1'),
        (r'</?p[^>]*>', '\n\n'),
        (r'</?span[^>]*>', '\n\n'),
        (r'</?table[^>]*>', '\n\n'),
        (r'</tr[^>]*>', ''),
        (r'<tr[^>]*>', '\n'),
        (r'</?td[^>]*>', ''),
        (r'\n{3,}', '\n\n'),
        (r'\*{4}', ' '),
    ])

    md = replaces.replace(html)
    if md[0] == '\n':
        md = md[1:]

    (dst_path / 'index.md').write_text(md, encoding='utf-8')

    return dict(
        unknown_colors=unknown_colors
    )
