from django.contrib.auth import get_user_model
from django.db.models import OneToOneField, CharField, Model, CASCADE
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(Model):
    user = OneToOneField(get_user_model(), blank=True, null=True, verbose_name='Пользователь', related_name='profile', on_delete=CASCADE)
    token39 = CharField('Token39', max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


@receiver(post_save, sender=get_user_model())
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=get_user_model())
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
