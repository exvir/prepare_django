from django.contrib.auth.forms import UserChangeForm


class CustomProfileChangeForm(UserChangeForm):
    class Meta:
        fields = '__all__'
