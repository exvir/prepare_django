# -*- coding: utf-8 -*-

import locale
import logging

from .common import *



CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'prepare_django',
        'USER': 'root',
        'PASSWORD': 'qscft813813',
        'HOST': 'localhost',
        'OPTIONS': {
            'charset': 'utf8mb4',  # <--- Use this
        }
    }
}

INSTALLED_APPS += [
    'drf_yasg',
    # 'rest_framework_swagger',
    # 'django.contrib.staticfiles',
    # 'debug_toolbar',

]
ALLOWED_HOSTS = ['192.168.57.68', 'localhost', '127.0.0.1', 'spnavigator.ru', 'prepare.spnavigator.ru']
'''
LOGGING = {
    'version': 1,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'sql.log': {
            'level': 'DEBUG',
            'class': 'core.logging.Utf8FileHandler',
            'filename': BASE_DIR.parent / 'logs' / 'prepare-sql.log',
        },
    },
    'loggers': {
        'django.db.backends': {
            'level': 'DEBUG',
            # 'handlers': ['console'],
            'handlers': ['sql.log'],
        }
    }
}
'''
# # from fnmatch import fnmatch
# #
# #
# # class glob_list(list):
# #     def __contains__(self, key):
# #         for elt in self:
# #             if fnmatch(key, elt): return True
# #         return False
# #
# #
# # INTERNAL_IPS = glob_list([
# #     '127.0.0.1',
# #     '91.78.112.77'
# # ])
#
# INTERNAL_IPS = ['127.0.0.1', '91.78.112.77']
#
#
# def show_toolbar(request):
#     return True
#
#
# DEBUG_TOOLBAR_CONFIG = {
#     "SHOW_TOOLBAR_CALLBACK": show_toolbar,
# }

SPNAVIGATOR_BASE = 'http://localhost:8000{url}'
SPNAVIGATOR_API = SPNAVIGATOR_BASE.format(url='/api/v1/{url}')
