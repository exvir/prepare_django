from django.conf import settings
from django.contrib import admin
from django.urls import path, include

from main.views import Home, Editor

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', Home.as_view(), name='home'),
    path('editor', Editor.as_view(), name='editor'),
    path('api/v1/', include('prepare_django.api.urls')),
    path('doc/', include('doc.urls')),
] 

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
