from django.conf import settings
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

app_name = 'api'

urlpatterns = [
    path('preview/', include('preview.api.urls')),
    path('doc/', include('doc.api.urls')),
]

if settings.DEBUG:
    from django.conf.urls import url

    schema_view = get_schema_view(
        openapi.Info(
            title="Prepare.SPNavigator API",
            default_version='v1',
            description="API for prepare.spnavigator.ru",
            terms_of_service="https://www.google.com/policies/terms/",
            contact=openapi.Contact(email="it@spraesens.ru"),
            license=openapi.License(name="StatusPraesens License"),
        ),
        public=True,
        permission_classes=(permissions.IsAdminUser,),
    )

    urlpatterns += [
        url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
        url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]
